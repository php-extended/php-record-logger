<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use Psr\Log\LoggerInterface;

/**
 * RecordComparatorLogger class file.
 * 
 * This class is a record comparator that logs every comparison.
 * 
 * @author Anastaszor
 */
class RecordComparatorLogger implements RecordComparatorInterface
{
	
	/**
	 * The comparator.
	 * 
	 * @var RecordComparatorInterface
	 */
	protected RecordComparatorInterface $_comparator;
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new RecordComparatorLogger with the given comparator and logger.
	 * 
	 * @param RecordComparatorInterface $comparator
	 * @param LoggerInterface $logger
	 */
	public function __construct(RecordComparatorInterface $comparator, LoggerInterface $logger)
	{
		$this->_comparator = $comparator;
		$this->_logger = $logger;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\RecordComparatorInterface::compare()
	 */
	public function compare(RecordInterface $record1, RecordInterface $record2) : float
	{
		$res = $this->_comparator->compare($record1, $record2);
		
		$this->_logger->info('Comparing {r1n}\\{r1c}::{r1i} with {r2n}\\{r2c}::{r1i} : {res}', [
			'r1n' => $record1->getNamespace(), 'r1c' => $record1->getClassname(), 'r1i' => $record1->getIdentifier(),
			'r2n' => $record2->getNamespace(), 'r2c' => $record2->getClassname(), 'r2i' => $record2->getIdentifier(),
			'res' => $res,
		]);
		
		return $res;
	}
	
}
