<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use ArrayIterator;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * RecordProviderLoggerList class file.
 * 
 * This class is a simple implementation of the RecordProviderListInterface.
 * 
 * @author Anastaszor
 * @extends \ArrayIterator<integer, RecordProviderInterface>
 */
class RecordProviderLoggerList extends ArrayIterator implements RecordProviderListInterface
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new RecordProviderLoggerList with the given logger and record
	 * providers.
	 * 
	 * @param array<integer, RecordProviderInterface> $values
	 * @param LoggerInterface $logger
	 */
	public function __construct(array $values = [], ?LoggerInterface $logger = null)
	{
		parent::__construct($values);
		if(null === $logger)
		{
			$logger = new NullLogger();
		}
		$this->_logger = $logger;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\RecordProviderListInterface::current()
	 */
	public function current() : RecordProviderInterface
	{
		/** @var RecordProviderInterface $recordProvider */
		$recordProvider = parent::current();
		$this->_logger->info(
			'Retrieving Record Provider {class}',
			['class' => \get_class($recordProvider)],
		);
		
		return $recordProvider;
	}
	
}
