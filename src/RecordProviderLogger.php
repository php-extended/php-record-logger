<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use InvalidArgumentException;
use Iterator;
use Psr\Log\LoggerInterface;

/**
 * RecordProviderLogger class file.
 * 
 * This class is a record provider that logs record calls.
 * 
 * @author Anastaszor
 */
class RecordProviderLogger implements RecordProviderInterface
{
	
	/**
	 * The provider.
	 * 
	 * @var RecordProviderInterface
	 */
	protected RecordProviderInterface $_provider;
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * Builds a new RecordProviderLogger with the given provider and logger.
	 * 
	 * @param RecordProviderInterface $provider
	 * @param LoggerInterface $logger
	 */
	public function __construct(RecordProviderInterface $provider, LoggerInterface $logger)
	{
		$this->_provider = $provider;
		$this->_logger = $logger;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\RecordProviderInterface::getAllRecords()
	 */
	public function getAllRecords(?string $namespace = null, ?string $classname = null) : Iterator
	{
		$this->_logger->info(
			'Retrieving all records {ns}\\{cls}',
			['ns' => $namespace, 'cls' => $classname],
		);
		
		return $this->_provider->getAllRecords($namespace, $classname);
	}
	
	/**
	 * Gets whether the record with the given namespace, classname, identifier
	 * exists and may be retrieved with the self::getRecord() method.
	 * 
	 * @param string $namespace
	 * @param string $classname
	 * @param string $identifier
	 * @return bool
	 */
	public function hasRecord(string $namespace, string $classname, string $identifier) : bool
	{
		$res = $this->_provider->hasRecord($namespace, $classname, $identifier);
		$this->_logger->info(
			'Checking if has record {ns}\\{cls}::{id} : {res}',
			['ns' => $namespace, 'cls' => $classname, 'id' => $identifier, 'res' => ($res ? 'yes' : 'no')],
		);
		
		return $res;
	}
	
	/**
	 * Gets the given record for the given namespace, classname and identifier.
	 * 
	 * @param string $namespace
	 * @param string $classname
	 * @param string $identifier
	 * @return RecordInterface
	 * @throws InvalidArgumentException if such record does not exists
	 */
	public function getRecord(string $namespace, string $classname, string $identifier) : RecordInterface
	{
		try
		{
			$res = $this->_provider->getRecord($namespace, $classname, $identifier);
			$this->_logger->info(
				'Fetching record {ns}\\{cls}::{id}',
				['ns' => $namespace, 'cls' => $classname, 'id' => $identifier],
			);
		}
		catch(InvalidArgumentException $e)
		{
			$this->_logger->info(
				'Fetching record {ns}\\{cls}::{id} : NOT FOUND',
				['ns' => $namespace, 'cls' => $classname, 'id' => $identifier],
			);
			
			throw $e;
		}
		
		return $res;
	}
	
}
