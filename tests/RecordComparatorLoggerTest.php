<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Record\RecordComparatorInterface;
use PhpExtended\Record\RecordComparatorLogger;
use PhpExtended\Record\RecordInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

/**
 * RecordComparatorLoggerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Record\RecordComparatorLogger
 *
 * @internal
 *
 * @small
 */
class RecordComparatorLoggerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var RecordComparatorLogger
	 */
	protected RecordComparatorLogger $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$comparator = new class() implements RecordComparatorInterface
		{
			
			public function __toString() : string
			{
				return self::class.'@'.\spl_object_hash($this);
			}
			
			public function compare(RecordInterface $record1, RecordInterface $record2) : float
			{
				return 0.0;
			}
			
		};
		
		$this->_object = new RecordComparatorLogger($comparator, new NullLogger());
	}
	
}
