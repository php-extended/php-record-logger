<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-logger library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Record\RecordInterface;
use PhpExtended\Record\RecordProviderInterface;
use PhpExtended\Record\RecordProviderLogger;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

/**
 * RecordProviderLoggerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Record\RecordProviderLogger
 *
 * @internal
 *
 * @small
 */
class RecordProviderLoggerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var RecordProviderLogger
	 */
	protected RecordProviderLogger $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$provider = new class() implements RecordProviderInterface
		{
			
			public function __toString() : string
			{
				return self::class.'@'.\spl_object_hash($this);
			}
			
			public function getAllRecords(?string $namespace, ?string $classname) : Iterator
			{
				return new ArrayIterator();
			}
			
			public function hasRecord(string $namespace, string $classname, string $identifier) : bool
			{
				return false;
			}
			
			public function getRecord(string $namespace, string $classname, string $identifier) : RecordInterface
			{
				throw new InvalidArgumentException();
			}
			
		};
		
		$this->_object = new RecordProviderLogger($provider, new NullLogger());
	}
	
}
