# php-extended/php-record-logger
A library that implements php-record-interface library for logging purposes.

![coverage](https://gitlab.com/php-extended/php-record-logger/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-record-logger/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-record-logger ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpExtended\Record\RecordComparatorLogger;

/* @var $comparator \PhpExtended\Record\RecordComparatorInterface */
/* @var $logger \Psr\Log\LoggerInterface */

$comparator = new RecordComparatorLogger($comparator, $logger);


```

And for the record provider :

```php

use PhpExtended\Record\RecordProviderLogger;

/* @var $provider \PhpExtended\Record\RecordProviderInterface */
/* @var $logger \Psr\Log\LoggerInterface */

$comparator = new RecordProviderLogger($provider, $logger);

```


## License

MIT (See [license file](LICENSE)).
